#pragma once

#include <await/coroutine/routine.hpp>
#include <await/executors/executor.hpp>

#include <await/fibers/core/id.hpp>

#include <optional>
#include <string>

namespace await::fibers {

using executors::IExecutorPtr;

using FiberRoutine = coroutine::Routine;

// Cooperative fibers
// Base fibers API

void Spawn(FiberRoutine routine, IExecutorPtr executor);

// Spawn new fiber in current executor
// Precondition: self::AmIFiber() == true
void Spawn(FiberRoutine routine);

namespace self {

// Reschedule current fiber
void Yield();

// Reschedule current fiber to executor `target`
void TeleportTo(IExecutorPtr target);

FiberId GetId();

std::optional<std::string> GetName();
void SetName(const std::string& name);

}  // namespace self

// Fiber or thread?
bool AmIFiber();

}  // namespace await::fibers
