#include <await/fibers/core/fls.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers::self {

void SetLocal(const std::string& key, std::any value) {
  auto& fls = Fiber::AccessCurrent()->GetFLS();
  fls.emplace(key, std::move(value));
}

std::any GetLocalImpl(const std::string& key) {
  auto& fls = Fiber::AccessCurrent()->GetFLS();

  if (auto it = fls.find(key); it != fls.end()) {
    return it->second;
  }
  return {};
}

}  // namespace await::fibers::self
