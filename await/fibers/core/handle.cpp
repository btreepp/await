#include <await/fibers/core/handle.hpp>

#include <await/fibers/core/fiber.hpp>

#include <wheels/support/assert.hpp>

namespace await::fibers {

void FiberHandle::Resume() {
  WHEELS_VERIFY(IsValid(), "Invalid fiber handle");
  f_->Resume();
}

}  // namespace await::fibers
