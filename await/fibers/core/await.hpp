#pragma once

#include <await/fibers/core/awaiter.hpp>
#include <await/fibers/core/suspend.hpp>

#include <utility>

namespace await::fibers {

// https://lewissbaker.github.io/2017/11/17/understanding-operator-co-await

template <typename Awaitable>
auto Await(Awaitable&& a) {
  auto awaiter = GetAwaiter(std::forward<Awaitable>(a));
  if (!awaiter.AwaitReady()) {
    Suspend(&awaiter);
  }
  return awaiter.GetResult();
}

}  // namespace await::fibers
