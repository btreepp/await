#include <await/fibers/core/id.hpp>

namespace await::fibers {

static wheels::IdGenerator ids;

FiberId GenerateNewId() {
  return ids.NextId();
}

void ResetIds() {
  ids.Reset();
}

}  // namespace await::fibers
