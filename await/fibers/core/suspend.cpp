#include <await/fibers/core/suspend.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

void Suspend(IAwaiter* awaiter) {
  Fiber::AccessCurrent()->Suspend(awaiter);
}

}  // namespace await::fibers
