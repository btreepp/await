#include <await/fibers/core/stack.hpp>

#include <await/support/thread_spinlock.hpp>

#include <array>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

static const size_t kStackSizePages = 8;

//////////////////////////////////////////////////////////////////////

// Requirement: no dynamic allocations (with operator `new`) in stack allocator!

// TODO: lock-free allocator

class StackAllocator {
  static const size_t kPoolLimit = 1024;

 public:
  Stack Allocate() {
    {
      std::lock_guard g(lock_);

      if (pool_size_ > 0) {
        return std::move(pool_[--pool_size_]);
      }
    }
    return AllocateNewStack();
  }

  void Release(Stack&& stack) {
    {
      std::lock_guard g(lock_);

      if (pool_size_ < kPoolLimit) {
        pool_[pool_size_++] = std::move(stack);
        return;
      }
    }

    ReleaseMemory(std::move(stack));
  }

 private:
  Stack AllocateNewStack() {
    return Stack::Allocate(kStackSizePages);
  }

  void ReleaseMemory(Stack&& stack) {
    Stack released(std::move(stack));
  }

 private:
  support::ThreadSpinLock lock_;
  std::array<Stack, kPoolLimit> pool_;
  size_t pool_size_{0};
};

//////////////////////////////////////////////////////////////////////

static StackAllocator allocator;

Stack AllocateStack() {
  // return Stack::Allocate(kStackSizePages);
  return allocator.Allocate();
}

void ReleaseStack(Stack&& stack) {
  // Stack released{std::move(stack)};
  allocator.Release(std::move(stack));
}

}  // namespace await::fibers
