#pragma once

#include <await/fibers/core/api.hpp>

#include <wheels/support/assert.hpp>

#define I_EXPECT_AWAIT_FIBERS \
  WHEELS_VERIFY(::await::fibers::AmIFiber(), "Fiber expected, not thread")
