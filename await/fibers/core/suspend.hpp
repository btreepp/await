#pragma once

#include <await/fibers/core/awaiter.hpp>

namespace await::fibers {

void Suspend(IAwaiter* awaiter);

}  // namespace await::fibers
