#include <await/fibers/core/api.hpp>

#include <await/fibers/core/fiber.hpp>

#include <wheels/support/assert.hpp>

namespace await::fibers {

void Spawn(FiberRoutine routine, IExecutorPtr executor) {
  Fiber::Spawn(std::move(routine), std::move(executor));
}

void Spawn(FiberRoutine routine) {
  Spawn(std::move(routine), Fiber::AccessCurrent()->GetExecutor());
}

namespace self {

void Yield() {
  Fiber::AccessCurrent()->Yield();
}

void TeleportTo(IExecutorPtr target) {
  Fiber::AccessCurrent()->TeleportTo(std::move(target));
}

FiberId GetId() {
  return Fiber::AccessCurrent()->Id();
}

void SetName(const std::string& name) {
  Fiber::AccessCurrent()->SetName(name);
}

std::optional<std::string> GetName() {
  return Fiber::AccessCurrent()->Name();
}

}  // namespace self

bool AmIFiber() {
  return Fiber::GetCurrent() != nullptr;
}

}  // namespace await::fibers
