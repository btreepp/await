#pragma once

#include <await/fibers/test/test_runtime.hpp>

#include <wheels/test/test_framework.hpp>

#define SIMPLE_FIBER_TEST(name, threads)           \
  void FiberTest##name(void);                      \
  SIMPLE_TEST(name) {                              \
    ::await::fibers::TestRuntime runtime{threads}; \
    runtime.Spawn([]() { FiberTest##name(); });    \
    runtime.Join();                                \
  }                                                \
  void FiberTest##name()
