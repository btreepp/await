#pragma once

#include <await/fibers/core/handle.hpp>
#include <await/fibers/core/suspend.hpp>

#include <await/support/thread_spinlock.hpp>
#include <await/support/mutex_holder.hpp>

#include <twist/stdlike/atomic.hpp>

#include <wheels/support/intrusive_list.hpp>

namespace await::fibers {

// https://eli.thegreenplace.net/2018/basics-of-futexes/

template <typename T>
class FutexLike : public twist::stdlike::atomic<T> {
 private:
  using Mutex = support::ThreadSpinLock;
  using UniqueLock = support::MutexHolder<Mutex>;

  class Waiter : public IAwaiter, public wheels::IntrusiveListNode<Waiter> {
   public:
    Waiter(UniqueLock&& lock) : lock_(std::move(lock)) {
    }

    void ScheduleResume(FiberHandle f) override {
      fiber_ = f;
      lock_.unlock();
    }

    void Resume() {
      fiber_.Resume();
    }

    FiberHandle Handle() {
      return fiber_;
    }

   private:
    UniqueLock lock_;
    FiberHandle fiber_;
  };

  using WaitList = wheels::IntrusiveList<Waiter>;

 public:
  explicit FutexLike(T initial) : twist::stdlike::atomic<T>(initial) {
  }

  ~FutexLike() {
    WHEELS_VERIFY(waiters_.IsEmpty(), "Waiters list is not empty");
  }

  // Park current fiber if value of atomic is equal to `expected`
  void ParkIf(T expected) {
    UniqueLock lock(mutex_);

    if (this->load() == expected) {
      Waiter waiter(std::move(lock));
      waiters_.PushBack(&waiter);
      Suspend(&waiter);
    }
  }

  void WakeOne() {
    FiberHandle f = TryTakeOne();
    if (f.IsValid()) {
      f.Resume();
    }
  }

  void WakeAll() {
    WaitList waiters = TakeAll();
    while (!waiters.IsEmpty()) {
      // NB: Pop from intrusive list before Resume
      Waiter* waiter = waiters.PopFront();
      waiter->Resume();
    }
  }

 private:
  FiberHandle TryTakeOne() {
    UniqueLock lock(mutex_);
    if (!waiters_.IsEmpty()) {
      Waiter* waiter = waiters_.PopFront();
      return waiter->Handle();
    }
    return FiberHandle::Invalid();
  }

  WaitList TakeAll() {
    UniqueLock lock(mutex_);
    return std::move(waiters_);
  }

 private:
  Mutex mutex_;
  WaitList waiters_;
};

}  // namespace await::fibers
