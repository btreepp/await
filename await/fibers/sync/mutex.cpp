#include <await/fibers/sync/mutex.hpp>

#include <await/fibers/core/api.hpp>
#include <await/fibers/core/check.hpp>

namespace await::fibers {

void Mutex::Lock() {
  I_EXPECT_AWAIT_FIBERS;

  while (locked_.exchange(true)) {
    locked_.ParkIf(true);
  }
#ifndef NDEBUG
  owner_id_ = self::GetId();
#endif
}

bool Mutex::TryLock() {
  if (!locked_.load() && !locked_.exchange(true)) {
#ifndef NDEBUG
    owner_id_ = self::GetId();
#endif
    return true;
  }
  return false;
}

void Mutex::Unlock() {
  I_EXPECT_AWAIT_FIBERS;

#ifndef NDEBUG
  WHEELS_VERIFY(OwnedByThisFiber(), "Current fiber != owner");
  owner_id_ = kInvalidFiberId;
#endif

  locked_.store(false);
  locked_.WakeOne();
}

#ifndef NDEBUG
bool Mutex::OwnedByThisFiber() const {
  return owner_id_ == self::GetId();
}
#endif

}  // namespace await::fibers
