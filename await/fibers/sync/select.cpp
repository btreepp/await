#include <await/fibers/sync/select.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

namespace detail {

size_t GetSelectTwister() {
  return Fiber::AccessCurrent()->GetSelectTwister();
}

}  // namespace detail

}  // namespace await::fibers
