#include <await/fibers/sync/teleport.hpp>

#include <await/fibers/core/fiber.hpp>

namespace await::fibers {

TeleportGuard::TeleportGuard(const IExecutorPtr& e) {
  home_ = Fiber::AccessCurrent()->GetExecutor();
  self::TeleportTo(e);
}

TeleportGuard::~TeleportGuard() {
  self::TeleportTo(home_);
}

}  // namespace await::fibers
