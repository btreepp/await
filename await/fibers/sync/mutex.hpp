#pragma once

#include <await/fibers/sync/futex.hpp>

#include <await/fibers/core/id.hpp>

#include <await/support/thread_safety.hpp>

// std::lock_guard
#include <mutex>

namespace await::fibers {

class AWAIT_CAPABILITY("mutex") Mutex {
  using ScopeGuard = std::lock_guard<Mutex>;

 public:
  void Lock() AWAIT_ACQUIRE();
  bool TryLock() AWAIT_TRY_ACQUIRE(true);
  void Unlock() AWAIT_RELEASE();

  // Usage: auto guard = mutex.Guard();
  ScopeGuard Guard() {
    return ScopeGuard(*this);
  }

#ifndef NDEBUG
  bool OwnedByThisFiber() const AWAIT_ASSERT_CAPABILITY(this);
#endif

  // Lockable
  // https://en.cppreference.com/w/cpp/named_req/Lockable
  // Be compatible with std::lock_guard and std::unique_lock

  // NOLINTNEXTLINE
  void lock() AWAIT_ACQUIRE() {
    Lock();
  }

  // NOLINTNEXTLINE
  bool try_lock() AWAIT_TRY_ACQUIRE(true) {
    return TryLock();
  }

  // NOLINTNEXTLINE
  void unlock() AWAIT_RELEASE() {
    Unlock();
  }

 private:
  FutexLike<bool> locked_{false};
#ifndef NDEBUG
  FiberId owner_id_{kInvalidFiberId};
#endif
};

}  // namespace await::fibers
