#pragma once

#include <await/fibers/core/api.hpp>

#include <twist/stdlike/atomic.hpp>

// std::lock_guard
#include <mutex>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

class SpinWait {
  static const size_t kYieldThreshold = 159;

 public:
  void operator()() {
    if (spins_++ > kYieldThreshold) {
      self::Yield();
    } else {
      // Pause?
    }
  }

 private:
  size_t spins_{0};
};

//////////////////////////////////////////////////////////////////////

class SpinLock {
  using ScopeGuard = std::lock_guard<SpinLock>;

 public:
  void Lock() {
    SpinWait spin_wait;
    while (locked_.exchange(true)) {
      while (locked_.load()) {
        spin_wait();
      }
    }
  }

  bool TryLock() {
    return !locked_.exchange(true);
  }

  void Unlock() {
    locked_.store(false);
  }

  // Usage: auto guard = spinlock.Guard();
  ScopeGuard Guard() {
    return ScopeGuard(*this);
  }

  // Lockable
  // https://en.cppreference.com/w/cpp/named_req/Lockable
  // Be compatible with std::lock_guard and std::unique_lock

  // NOLINTNEXTLINE
  void lock() {
    Lock();
  }

  // NOLINTNEXTLINE
  bool try_lock() {
    return TryLock();
  }

  // NOLINTNEXTLINE
  void unlock() {
    Unlock();
  }

 private:
  twist::stdlike::atomic<bool> locked_{false};
};

}  // namespace await::fibers
