#include <await/fibers/sync/condvar.hpp>

#include <await/fibers/core/check.hpp>

namespace await::fibers {

void CondVar::Wait(Lock& lock) {
  I_EXPECT_AWAIT_FIBERS;

  size_t count = count_.load();
  lock.unlock();
  count_.ParkIf(count);
  lock.lock();
}

void CondVar::NotifyOne() {
  I_EXPECT_AWAIT_FIBERS;

  count_.fetch_add(1);
  count_.WakeOne();
}

void CondVar::NotifyAll() {
  I_EXPECT_AWAIT_FIBERS;

  count_.fetch_add(1);
  count_.WakeAll();
}

}  // namespace await::fibers
