#pragma once

#include <await/executors/executor.hpp>

namespace await::executors {

struct IManualExecutor : public IExecutor {
  virtual ~IManualExecutor() = default;

  // Executes next task and returns true
  // Returns false if task queue is empty
  virtual bool MakeStep() = 0;

  virtual size_t MakeSteps(size_t count) = 0;
};

using IManualExecutorPtr = std::shared_ptr<IManualExecutor>;

IManualExecutorPtr MakeManualExecutor();

}  // namespace await::executors
