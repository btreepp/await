#include <await/executors/static_thread_pool.hpp>

#include <await/executors/label_thread.hpp>
#include <await/executors/helpers.hpp>

#include <await/support/queues.hpp>

#include <twist/stdlike/thread.hpp>
#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/condition_variable.hpp>

namespace await::executors {

class StaticThreadPool final : public IThreadPool {
 public:
  StaticThreadPool(size_t threads, const std::string& name) : name_(name) {
    LaunchWorkerThreads(threads);
  }

  ~StaticThreadPool() {
    Shutdown();
  }

  // IExecutor

  void Execute(Task&& task) override {
    WorkCreated();
    tasks_.Put(std::move(task));
  }

  // IThreadPool

  void Join() override {
    joining_.store(true);
    if (work_count_.load() == 0) {
      tasks_.Close();
    }
    JoinWorkers();
  }

  void Shutdown() override {
    tasks_.Shutdown();
    JoinWorkers();
  }

  size_t ExecutedTaskCount() const override {
    return executed_count_.load();
  }

 private:
  void LaunchWorkerThreads(size_t threads) {
    for (size_t i = 0; i < threads; ++i) {
      workers_.emplace_back([this]() { WorkerRoutine(); });
    }
  }

  void WorkerRoutine() {
    LabelThread(name_);
    while (auto task = tasks_.Take()) {
      SafelyExecuteHere(task.value());
      WorkCompleted();
      executed_count_.fetch_add(1);
    }
  }

  void WorkCreated() {
    work_count_.fetch_add(1);
  }

  void WorkCompleted() {
    if (work_count_.fetch_sub(1) == 1 && joining_.load()) {
      tasks_.Close();
    }
  }

  void JoinWorkers() {
    for (auto& worker : workers_) {
      worker.join();
    }
    workers_.clear();
  }

 private:
  std::string name_;
  support::MPMCBlockingQueue<Task> tasks_;
  std::vector<twist::stdlike::thread> workers_;
  twist::stdlike::atomic<size_t> work_count_{0};

  twist::stdlike::atomic<size_t> executed_count_{0};

  twist::stdlike::atomic<bool> joining_{false};
};

IThreadPoolPtr MakeStaticThreadPool(size_t threads, const std::string& name) {
  return std::make_shared<StaticThreadPool>(threads, name);
}

}  // namespace await::executors
