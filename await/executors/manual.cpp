#include <await/executors/manual.hpp>

#include <deque>

namespace await::executors {

class ManualExecutor : public IManualExecutor {
 public:
  void Execute(Task&& task) override {
    tasks_.push_back(std::move(task));
  }

  bool MakeStep() override {
    if (tasks_.empty()) {
      return false;
    }
    auto task = std::move(tasks_.front());
    tasks_.pop_front();
    task();
    return true;
  }

  size_t MakeSteps(size_t count) override {
    for (size_t i = 0; i < count; ++i) {
      if (!MakeStep()) {
        return i;
      }
    }
    return count;
  }

 private:
  std::deque<Task> tasks_;
};

IManualExecutorPtr MakeManualExecutor() {
  return std::make_shared<ManualExecutor>();
}

}  // namespace await::executors
