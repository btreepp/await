#include <await/executors/strand.hpp>

#include <await/executors/helpers.hpp>
#include <await/support/queues.hpp>
#include <await/support/thread_spinlock.hpp>

namespace await::executors {

class Strand : public IExecutor, public std::enable_shared_from_this<Strand> {
 public:
  explicit Strand(IExecutorPtr executor) : underlying_(executor) {
  }

  void Execute(Task&& task) override {
    pending_tasks_.Put(std::move(task));
    TryScheduleBatch();
  }

 private:
  void TryScheduleBatch() {
    if (lock_.TryLock()) {
      ScheduleBatch();
    }
  }

  void ScheduleBatch() {
    underlying_->Execute([this, self = shared_from_this()] {
      ExecuteBatch();
      lock_.Unlock();
      if (!pending_tasks_.IsEmpty()) {
        TryScheduleBatch();
      }
    });
  }

  void ExecuteBatch() {
    auto todo = pending_tasks_.TakeAll();
    for (auto&& task : todo) {
      SafelyExecuteHere(task);
    }
  }

 private:
  IExecutorPtr underlying_{nullptr};
  support::MPSCLockFreeQueue<Task> pending_tasks_;
  support::ThreadSpinLock lock_;
};

////////////////////////////////////////////////////////////////////////////////

IExecutorPtr MakeStrand(IExecutorPtr executor) {
  return std::make_shared<Strand>(executor);
}

}  // namespace await::executors
