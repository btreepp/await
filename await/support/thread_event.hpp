#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

namespace await::support {

class ThreadOneShotEvent {
 public:
  void Set() {
    std::lock_guard guard(mutex_);
    set_ = true;
    set_cond_.notify_all();
  }

  void Await() {
    std::unique_lock lock(mutex_);
    while (!set_) {
      set_cond_.wait(lock);
    }
  }

 private:
  twist::stdlike::mutex mutex_;
  twist::stdlike::condition_variable set_cond_;
  bool set_{false};
};

}  // namespace await::support
