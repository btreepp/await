// https://clang.llvm.org/docs/ThreadSafetyAnalysis.html#mutexheader

#ifndef THREAD_SAFETY_ANALYSIS_MUTEX_H
#define THREAD_SAFETY_ANALYSIS_MUTEX_H

// Enable thread safety attributes only with clang.
// The attributes can be safely erased when compiling with other compilers.
#if defined(__clang__) && (!defined(SWIG))
#define AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(x)   __attribute__((x))
#else
#define AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(x)   // no-op
#endif

#define AWAIT_CAPABILITY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(capability(x))

#define AWAIT_SCOPED_CAPABILITY \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(scoped_lockable)

#define AWAIT_GUARDED_BY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(guarded_by(x))

#define AWAIT_PT_GUARDED_BY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(pt_guarded_by(x))

#define AWAIT_ACQUIRED_BEFORE(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(acquired_before(__VA_ARGS__))

#define AWAIT_ACQUIRED_AFTER(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(acquired_after(__VA_ARGS__))

#define AWAIT_REQUIRES(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(requires_capability(__VA_ARGS__))

#define AWAIT_REQUIRES_SHARED(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(requires_shared_capability(__VA_ARGS__))

#define AWAIT_ACQUIRE(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(acquire_capability(__VA_ARGS__))

#define AWAIT_ACQUIRE_SHARED(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(acquire_shared_capability(__VA_ARGS__))

#define AWAIT_RELEASE(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(release_capability(__VA_ARGS__))

#define AWAIT_RELEASE_SHARED(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(release_shared_capability(__VA_ARGS__))

#define AWAIT_RELEASE_GENERIC(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(release_generic_capability(__VA_ARGS__))

#define AWAIT_TRY_ACQUIRE(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(try_acquire_capability(__VA_ARGS__))

#define AWAIT_TRY_ACQUIRE_SHARED(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(try_acquire_shared_capability(__VA_ARGS__))

#define AWAIT_EXCLUDES(...) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(locks_excluded(__VA_ARGS__))

#define AWAIT_ASSERT_CAPABILITY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(assert_capability(x))

#define AWAIT_ASSERT_SHARED_CAPABILITY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(assert_shared_capability(x))

#define AWAIT_RETURN_CAPABILITY(x) \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(lock_returned(x))

#define AWAIT_NO_THREAD_SAFETY_ANALYSIS \
  AWAIT_THREAD_ANNOTATION_ATTRIBUTE__(no_thread_safety_analysis)

#endif  // THREAD_SAFETY_ANALYSIS_MUTEX_H
