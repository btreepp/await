#pragma once

#include <twist/stdlike/mutex.hpp>

#include <optional>
#include <vector>

namespace await::support {

template <typename T>
class Pool {
 public:
  std::optional<T> TryTake() {
    std::lock_guard guard{mutex_};
    if (!pool_.empty()) {
      T item = std::move(pool_.back());
      pool_.pop_back();
      return item;
    } else {
      return {};
    }
  }

  void Put(T item) {
    std::lock_guard guard{mutex_};
    pool_.push_back(std::move(item));
  }

 private:
  std::vector<T> pool_;
  twist::stdlike::mutex mutex_;
};

}  // namespace await::support
