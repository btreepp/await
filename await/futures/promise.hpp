#pragma once

#include <await/futures/state.hpp>
#include <await/futures/future.hpp>

#include <wheels/support/result.hpp>

#include <memory>

namespace await::futures {

using wheels::Error;
using wheels::Result;

template <typename T>
class PromiseBase : public detail::HoldState<T> {
  using detail::HoldState<T>::state_;
  using detail::HoldState<T>::CheckState;
  using detail::HoldState<T>::ReleaseState;

 public:
  PromiseBase()
      : detail::HoldState<T>(std::make_shared<detail::SharedState<T>>()) {
  }

  // One-shot
  Future<T> MakeFuture() {
    WHEELS_VERIFY(!future_extracted_, "Future already extracted");
    future_extracted_ = true;
    return Future{state_};
  }

  void SetError(Error error) && {
    ReleaseState()->SetResult(wheels::make_result::Fail(std::move(error)));
  }

  void Set(Result<T> result) && {
    ReleaseState()->SetResult(std::move(result));
  }

 private:
  bool future_extracted_{false};
};

template <typename T>
class Promise : public PromiseBase<T> {
  using Base = PromiseBase<T>;

  using detail::HoldState<T>::ReleaseState;

 public:
  using Base::MakeFuture;
  using Base::Set;
  using Base::SetError;

  void SetValue(T value) && {
    ReleaseState()->SetResult(wheels::make_result::Ok(std::move(value)));
  }
};

template <>
class Promise<void> : public PromiseBase<void> {
  using Base = PromiseBase<void>;
  using detail::HoldState<void>::ReleaseState;

 public:
  using Base::MakeFuture;
  using Base::Set;
  using Base::SetError;

  void Set() && {
    ReleaseState()->SetResult(wheels::make_result::Ok());
  }
};

//////////////////////////////////////////////////////////////////////

template <typename T>
using Contract = std::pair<Future<T>, Promise<T>>;

// Usage:
// auto [f, p] = futures::MakeContract<T>();
// https://en.cppreference.com/w/cpp/language/structured_binding

template <typename T>
Contract<T> MakeContract() {
  Promise<T> p;
  auto f = p.MakeFuture();
  return {std::move(f), std::move(p)};
}

}  // namespace await::futures
