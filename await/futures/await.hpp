#pragma once

#include <await/fibers/core/api.hpp>
#include <await/fibers/core/await.hpp>
#include <await/fibers/sync/future.hpp>

#include <await/futures/future.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

// Fiber/thread-friendly await procedure
template <typename T>
Result<T> Await(Future<T>&& future) {
  // TODO: shortcut for ready future

  if (fibers::AmIFiber()) {
    return fibers::Await(std::move(future));
  } else {
    return std::move(future).GetResult();
  }
}

}  // namespace await::futures
