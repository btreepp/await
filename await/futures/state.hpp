#pragma once

#include <await/executors/executor.hpp>
#include <await/executors/helpers.hpp>
#include <await/executors/inline.hpp>

#include <wheels/support/function.hpp>
#include <wheels/support/result.hpp>

#include <twist/stdlike/atomic.hpp>

#include <optional>

namespace await::futures {

using executors::IExecutorPtr;
using wheels::Result;

namespace detail {

//////////////////////////////////////////////////////////////////////

// State shared between Promise and Future

template <typename T>
class SharedState {
  enum class State {
    Initial = 0,
    CallbackSet = 1,
    ResultSet = 2,
    ResultConsumed = 3,  // Terminal state
  };

 public:
  using Callback = wheels::UniqueFunction<void(Result<T>)>;

 public:
  SharedState() : executor_(executors::GetInlineExecutor()) {
  }

  void SetResult(Result<T>&& result) {
    result_.emplace(std::move(result));

    switch (state_.exchange(State::ResultSet)) {
      case State::CallbackSet:
        InvokeCallback();
        break;
      case State::Initial:
        // Do nothing
        break;
      default:
        WHEELS_PANIC("Unexpected state");
    }
  }

  bool HasResult() const {
    return state_.load() == State::ResultSet;
  }

  Result<T> GetReadyResult() {
    WHEELS_VERIFY(state_.exchange(State::ResultConsumed) == State::ResultSet,
                  "Future is not completed");
    return std::move(*result_);
  }

  void SetExecutor(IExecutorPtr e) {
    executor_ = std::move(e);
  }

  IExecutorPtr GetExecutor() const {
    return executor_;
  }

  void SetCallback(Callback callback) {
    callback_ = std::move(callback);

    switch (state_.exchange(State::CallbackSet)) {
      case State::Initial:
        // Do nothing
        break;
      case State::ResultSet:
        InvokeCallback();
        break;
      default:
        WHEELS_PANIC("Unexpected state");
    }
  }

 private:
  void InvokeCallback() {
    state_.store(State::ResultConsumed);
    executor_->Execute([result = std::move(*result_),
                        callback = std::move(callback_)]() mutable {
      callback(std::move(result));
    });
  }

 private:
  twist::stdlike::atomic<State> state_{State::Initial};
  std::optional<Result<T>> result_;
  Callback callback_;
  IExecutorPtr executor_;
};

//////////////////////////////////////////////////////////////////////

// Common base for Promise and Future

template <typename T>
using StateRef = std::shared_ptr<SharedState<T>>;

template <typename T>
class HoldState {
 protected:
  HoldState(StateRef<T> state) : state_(std::move(state)) {
  }

  // Movable
  HoldState(HoldState&& that) = default;
  HoldState& operator=(HoldState&& that) = default;

  // Non-copyable
  HoldState(const HoldState& that) = delete;
  HoldState& operator=(const HoldState& that) = delete;

  StateRef<T> ReleaseState() {
    CheckState();
    return std::move(state_);
  }

  bool HasState() const {
    return (bool)state_;
  }

  void CheckState() const {
    WHEELS_VERIFY(HasState(), "No shared state / shared state released");
  }

 protected:
  StateRef<T> state_;
};

}  // namespace detail

}  // namespace await::futures
