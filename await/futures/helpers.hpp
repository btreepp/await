#pragma once

#include <await/futures/future.hpp>
#include <await/futures/promise.hpp>

#include <twist/stdlike/atomic.hpp>

#include <system_error>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <typename F>
auto AsyncVia(const IExecutorPtr& e, F&& f) {
  using T = decltype(f());

  auto [af, ap] = MakeContract<T>();

  auto task = [p = std::move(ap), f = std::forward<F>(f)]() mutable {
    auto result = wheels::make_result::Invoke(f);
    std::move(p).Set(std::move(result));
  };

  e->Execute(std::move(task));

  return std::move(af).Via(e);
}

template <typename F>
auto SyncVia(const IExecutorPtr& e, F&& f) {
  return Await(AsyncVia(e, std::forward<F>(f))).ExpectOk();
}

//////////////////////////////////////////////////////////////////////

// Future<T> -> Future<void>

template <typename T>
Future<void> JustStatus(Future<T>&& f) {
  auto [jsf, jsp] = MakeContract<void>();
  auto e = f.GetExecutor();
  std::move(f).Subscribe([jsp = std::move(jsp)](Result<T> result) mutable {
    if (result.IsOk()) {
      std::move(jsp).Set();
    } else {
      // Propagate error
      std::move(jsp).SetError(result.GetError());
    }
  });
  return std::move(jsf).Via(e);
}

//////////////////////////////////////////////////////////////////////

template <typename T>
Future<T> MakeValue(T&& value) {
  auto [f, p] = MakeContract<T>();
  std::move(p).SetValue(std::forward<T>(value));
  return std::move(f);
}

//////////////////////////////////////////////////////////////////////

Future<void> MakeCompletedVoid();

//////////////////////////////////////////////////////////////////////

namespace detail {

class Failure {
 public:
  explicit Failure(Error&& error) : error_(std::move(error)) {
  }

  // Non-copyable
  Failure(const Failure& that) = delete;
  Failure& operator=(const Failure& that) = delete;

  template <typename T>
  Future<T> As() && {
    auto [f, p] = MakeContract<T>();
    std::move(p).SetError(std::move(error_));
    return std::move(f);
  }

  template <typename T>
  operator Future<T>() && {
    return std::move(*this).As<T>();
  }

 private:
  Error error_;
};

}  // namespace detail

// Usage:
// 1) Future<T> f = MakeError(e);
// 2) auto f = MakeError(e).As<T>();

detail::Failure MakeError(Error&& error);

//////////////////////////////////////////////////////////////////////

template <typename F, typename T>
Future<T> RecoverWith(Future<T>&& f, F&& fallback) {
  auto [rwf, rwp] = MakeContract<T>();

  auto e = f.GetExecutor();

  auto recover = [p = std::move(rwp), fallback = std::forward<F>(fallback)](
                     Result<T> result) mutable {
    if (result.IsOk()) {
      std::move(p).Set(std::move(result));
    } else {
      // Fallback
      Future<T> ff = fallback(result.GetError());
      std::move(ff).Subscribe([p = std::move(p)](Result<T> result) mutable {
        std::move(p).Set(std::move(result));
      });
    }
  };
  std::move(f).Subscribe(std::move(recover));

  return std::move(rwf).Via(e);
}

//////////////////////////////////////////////////////////////////////

template <typename T, typename Handler>
Future<T> SubscribeConst(Future<T>&& f, Handler&& handler) {
  auto e = f.GetExecutor();

  auto [_f, _p] = await::futures::MakeContract<T>();

  auto callback = [_p = std::move(_p), handler = std::forward<Handler>(
                                           handler)](Result<T> result) mutable {
    handler(static_cast<const Result<T>&>(result));

    // Forward result
    std::move(_p).Set(std::move(result));
  };

  std::move(f).Subscribe(std::move(callback));
  return std::move(_f).Via(e);
}

//////////////////////////////////////////////////////////////////////

namespace detail {

template <typename T>
class WithInterruptState {
 public:
  WithInterruptState(Promise<T> promise) : promise_(std::move(promise)) {
  }

  void Complete(Result<T> result) {
    if (!done_.exchange(true)) {
      std::move(promise_).Set(std::move(result));
    }
  }

  void Interrupt(Error error) {
    if (!done_.exchange(true)) {
      std::move(promise_).SetError(std::move(error));
    }
  }

 private:
  Promise<T> promise_;
  twist::stdlike::atomic<bool> done_{false};
};

}  // namespace detail

template <typename T>
Future<T> WithInterrupt(
    Future<T> f, Future<void> interrupt,
    std::error_code error = std::make_error_code(std::errc::interrupted)) {
  auto [fi, pi] = MakeContract<T>();

  auto e = f.GetExecutor();

  auto state = std::make_shared<detail::WithInterruptState<T>>(std::move(pi));

  // Consume f
  std::move(f).Subscribe(
      [state](Result<T> result) { state->Complete(std::move(result)); });

  // Consume interrupt
  std::move(interrupt).Subscribe(
      [state, error](Result<void> /*status*/) { state->Interrupt(error); });

  return std::move(fi).Via(e);
}

}  // namespace await::futures
