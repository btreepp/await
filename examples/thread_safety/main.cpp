#include <await/fibers/test/example.hpp>
#include <await/fibers/sync/mutex.hpp>

class ThreadSafeInt {
 public:
  ThreadSafeInt(int value)
    : value_(value) {
  }

  void Store(int value) {
    value_ = value;  // writing variable 'value_' requires holding mutex 'mutex_' exclusively
  }

  int Load() {
    mutex_.Lock();
    int value = value_;
    return value;
    // mutex 'mutex_' is still held at the end of function
  }

 private:
  await::fibers::Mutex mutex_;
  int value_ GUARDED_BY(mutex_);
};

// Intentionally does not compile!

AWAIT_EXAMPLE() {
  ThreadSafeInt shared_int{0};
  shared_int.Store(42);
  shared_int.Load();
}
