
set(EXAMPLE_TARGET await_example_thread_safety)

add_compile_options(-Wthread-safety -Werror)
add_executable(${EXAMPLE_TARGET} main.cpp)
target_link_libraries(${EXAMPLE_TARGET} await)
