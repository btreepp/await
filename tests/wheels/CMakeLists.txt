cmake_minimum_required(VERSION 3.5)

message(STATUS "Await tests")

# List sources

file(GLOB TEST_SOURCES "./*.cpp")

# All tests target

add_executable(await_tests ${TEST_SOURCES})
target_link_libraries(await_tests await)
